import glob
import math
import os
from numbers import Integral, Real


class Map(dict):
    """
    Example:
    m = Map({'first_name': 'Eduardo'}, last_name='Pool', age=24, sports=['Soccer'])
    Source:
    https://stackoverflow.com/questions/2352181/how-to-use-a-dot-to-access-members-of-dictionary
    """
    def __init__(self, *args, **kwargs):
        super(Map, self).__init__(*args, **kwargs)
        for arg in args:
            if isinstance(arg, dict):
                for k, v in arg.items():
                    self[k] = v

        if kwargs:
            for k, v in kwargs.items():
                self[k] = v

    def __getattr__(self, attr):
        return self.get(attr)

    def __setattr__(self, key, value):
        self.__setitem__(key, value)

    def __setitem__(self, key, value):
        super(Map, self).__setitem__(key, value)
        self.__dict__.update({key: value})

    def __delattr__(self, item):
        self.__delitem__(item)

    def __delitem__(self, key):
        super(Map, self).__delitem__(key)
        del self.__dict__[key]


ASSEMBLED = 'assembled'
MANUFACTURED = 'manufactured'
PURCHASED = 'purchased'
part_dict = {
    'component_type': ASSEMBLED,
    'is_root_component': True,
    'size_x': 1,
    'size_y': 1,
    'size_z': 1,
    'area': 1,
    'volume': 1,
    'qty': 1,
    'material': 'Aluminum 6061-T6',
    'material_family': 'Aluminum',
    'material_class': 'Metal',
    'weight': 1,
    'density': 1,
    'mat_cost_per_volume': 1,
    'mat_cost_per_area': 1,
    'mat_cut_rate': 1,
    'mat_pierce_time': 1,
    'mat_added_lead_time': 1,
    'purchased_children': 1,
    'manufactured_children': 1,
    'assembled_children': 1,
}
part = Map(part_dict)
INDEX = 0
NOQUOTE = None
number = 0
currency = 0
floor = math.floor
ceil = math.ceil


def analyze_additive():
    return Map({
        'runtime': 1,
        'orientation': 'x',
        'support_volume': 1,
        'material_volume': 1,
        'does_part_fit': True,
    })


def analyze_mill3():
    return Map({
        'runtime': 1,
        'setup_time': 1,
        'setup_count': 1,
        'confidence': 'High',
        'setups': [Map({'setup_time': 1, 'runtime': 1, 'confidence': 'High'})]
    })


def analyze_lathe():
    return Map({
        'runtime': 1,
        'setup_time': 1,
        'setup_count': 1,
        'stock_radius': 1,
        'stock_length': 1,
        'confidence': 'High',
        'setups': [Map({'setup_time': 1, 'runtime': 1, 'confidence': 'High'})]
    })


def analyze_casting():
    return Map({
        'pins': 1,
        'cores': 1,
        'custom_cores': 1,
        'side_actions': 1
    })


def analyze_sheet_metal():
    return Map({
        'bends': [Map({'radius': 1, 'angle': 1, 'length': 1, 'k_factor': 0.3})],
        'bend_count': 1,
        'thickness': 1,
        'size_x': 1,
        'size_y': 1,
        'total_cut_length': 1,
        'pierce_count': 1,
        'punch_single_hit_cut_length': 1,
        'punch_multi_hit_cut_length': 1,
        'punch_single_hit_setups': 1,
        'punch_single_hit_count': 1,
        'total_hole_setups': 1,
        'total_hole_count': 1,
        'counter_sink_setups': 1,
        'counter_sink_count': 1,
        'counter_bore_setups': 1,
        'counter_bore_count': 1,
        'simple_drilled_hole_setups': 1,
        'simple_drilled_hole_count': 1,
    })


def analyze_wire_edm():
    return Map({
        'runtime': 1,
        'setup_time': 1,
        'setup_count': 1,
        'setups': [Map({'setup_time': 1, 'runtime': 1, 'cut_length': 1, 'average_cut_depth': 1, 'pierce_count': 1})],
        'cut_length': 1,
        'pierce_count': 1,
        'average_cut_depth': 1,
    })


units_mm = lambda: None
units_in = lambda: None


class DynamicVariable(Real):
    """
    A mutable object that behaves like a Real. An instance is returned in P3L
    when calling the `var` method with `dynamic=True`. The instance's value
    can be changed via the `update` method, and the instance should always be
    frozen by calling `freeze()`, which applies an override when necessary.

    See https://www.python.org/dev/peps/pep-3141/ for more information on Real.
    """

    def __init__(self, float_value, override=None):
        self.float = float(float_value)
        self.frozen = False
        self.override = override

    def __bool__(self):
        """True if self != 0."""
        return self.float != 0

    def __add__(self, other):
        return self.float + other

    def __radd__(self, other):
        return self.float + other

    def __neg__(self):
        return -self.float

    def __pos__(self):
        """Coerces self to whatever class defines the method."""
        return self.float

    def __sub__(self, other):
        return self.float + -other

    def __rsub__(self, other):
        return -self.float + other

    def __mul__(self, other):
        return self.float * other

    def __rmul__(self, other):
        return self.float * other

    def __div__(self, other):
        """a/b; should promote to float or complex when necessary."""
        return self.float / other

    def __rdiv__(self, other):
        return other / self.float

    def __rtruediv__(self, other):
        return other / self.float

    def __truediv__(self, other):
        return self.float / other

    def __pow__(self, exponent):
        """a**b; should promote to float or complex when necessary."""
        return self.float ** exponent

    def __rpow__(self, base):
        return base ** self.float

    def __abs__(self):
        """Returns the Real distance from 0."""
        return abs(self.float)

    def __eq__(self, other):
        return self.float == other

    # __ne__ is inherited from object and negates whatever __eq__ does.

    def __float__(self):
        """Any Real can be converted to a native float object."""
        return self.float

    def __trunc__(self):
        """Truncates self to an Integral.

        Returns an Integral i such that:
          * i>=0 iff self>0;
          * abs(i) <= abs(self);
          * for any Integral j satisfying the first two conditions,
            abs(i) >= abs(j) [i.e. i has "maximal" abs among those].
        i.e. "truncate towards 0".
        """
        return int(self.float)

    def __floor__(self):
        """Finds the greatest Integral <= self."""
        return math.floor(self.float)

    def __ceil__(self):
        """Finds the least Integral >= self."""
        return math.ceil(self.float)

    def __round__(self, ndigits: Integral = None):
        """Rounds self to ndigits decimal places, defaulting to 0.

        If ndigits is omitted or None, returns an Integral,
        otherwise returns a Real, preferably of the same type as
        self. Types may choose which direction to round half. For
        example, float rounds half toward even.

        """
        return round(self.float, ndigits=ndigits)

    def __divmod__(self, other):
        """The pair (self // other, self % other).

        Sometimes this can be computed faster than the pair of
        operations.
        """
        return self.float // other, self.float % other

    def __rdivmod__(self, other):
        """The pair (self // other, self % other).

        Sometimes this can be computed faster than the pair of
        operations.
        """
        return other // self.float, other % self.float

    def __floordiv__(self, other):
        """The floor() of self/other. Integral."""
        return math.floor(self.float / other)

    def __rfloordiv__(self, other):
        """The floor() of other/self."""
        return math.floor(other / self.float)

    def __mod__(self, other):
        """self % other"""
        return self.float % other

    def __rmod__(self, other):
        """other % self"""
        return other % self.float

    def __lt__(self, other):
        """< on Reals defines a total ordering, except perhaps for NaN."""
        return self.float < other

    def __le__(self, other):
        return self.float <= other

    def __gt__(self, other):
        return self.float > other

    def __ge__(self, other):
        return self.float >= other

    # Concrete implementations of Complex abstract methods.
    # Subclasses may override these, but don't have to.

    def __complex__(self):
        return complex(float(self.float))

    @property
    def real(self):
        return +self.float

    def conjugate(self):
        """Conjugate is a no-op for Reals."""
        return +self.float

    def __hash__(self):
        return hash(self.float)

    def update(self, value):
        assert not self.frozen
        try:
            self.float = float(value)
        except (TypeError, ValueError):
            self.float = 0

    def freeze(self):
        self.frozen = True
        if self.override is not None:
            self.float = float(self.override)


def var(_, default_value, __, ___, **kwargs):
    if kwargs.get('frozen', True):
        return default_value
    else:
        return DynamicVariable(float_value=default_value)


def is_close(n1, n2, tol=1e-3):
    return abs(n2 - n1) < tol


def mock_function(*args, **kwargs):
    pass


def mock_iterator(obj, name='None'):
    return []


set_operation_name = mock_function
no_quote = mock_function
set_workpiece_value = mock_function
get_features = mock_iterator
get_feedback = mock_iterator
get_setups = mock_iterator
set_custom_attribute = mock_function
generate_operation = mock_function
mean = mock_function
median = mock_function


def get_workpiece_value(x, y):
    return y


def get_price_value(x):
    return 0


def get_operation_property(x, y):
    return y


def get_custom_attribute(x, y):
    return y


def get_allowed_operations():
    return ['Admin', 'QA', 'Milling']


operation_output_str = """
print('PRICE: {}'.format(PRICE))
print('DAYS: {}'.format(DAYS))
"""


def exec_operation_file(filename):
    with open(filename, 'r') as f:
        exec(f.read() + operation_output_str)


def exec_process_file(filename):
    with open(filename, 'r') as f:
        exec(f.read())


py_files = glob.glob('*/*.py')
py_files.extend(glob.glob('*/*/*.py'))
for py_file in py_files:
    if os.path.isdir(py_file):
        continue
    print('------------------------------')
    print(py_file)
    print('------------------------------')
    if 'process' in py_file:
        exec_process_file(py_file)
    else:
        exec_operation_file(py_file)

