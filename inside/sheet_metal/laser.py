# This operation is for flat cutting operations, such as a laser cutter, plasma cutter, or water jet.
# Contact Paperless Parts Customer Success to ask about getting custom values populated for your cut rates and
# pierce times.
#
# When working with runtime or setup_time in your pricing formula, time will always be in hours.  The display units
# can be set on the operation level or when quoting.

units_in()
sheet_metal = analyze_sheet_metal()

setup_time = var('setup_time', 0.5, 'Setup time, specified in hours', number)
runtime = var('runtime', 0, 'Runtime, specified in hours', number, frozen=False)

cut_length = var('cut length', 0, 'total laser cut length', number, frozen=False)
pierce_count = var('pierce count', 0, 'number of times laser needs to pierce', number, frozen=False)

cut_length.update(sheet_metal.total_cut_length)
cut_length.freeze()
pierce_count.update(sheet_metal.pierce_count)
pierce_count.freeze()

mat_cut_rate = var('Mat Cut Rate', 0, 'material cut rate in/min', number, frozen=False)
if part.mat_cut_rate:
    mat_cut_rate.update(part.mat_cut_rate)
else:
    # Default to 20 inches per minute
    mat_cut_rate.update(0.1)
mat_cut_rate.freeze()

mat_pierce_time = var('Mat Pierce Time', 0, 'material pierce time, seconds', number, frozen=False)
if part.mat_pierce_time:
    mat_pierce_time.update(part.mat_pierce_time)
else:
    # Default to 2 seconds
    mat_pierce_time.update(2)
mat_pierce_time.freeze()

# update runtime to sheet metal cutting perimeter
runtime.update(cut_length / mat_cut_rate / 60 + pierce_count * mat_pierce_time / 3600)
runtime.freeze()

labor_rate = var('Labor Rate', 100, 'Cost per hour for setup', currency)
machine_rate = var('Machine Rate', 50, 'Cost per hour for run', currency)

total_cycle_time = part.qty * runtime

setup_cost = labor_rate * setup_time
machine_cost = machine_rate * total_cycle_time

PRICE = setup_cost + machine_cost
DAYS = 0
